package kafka_pubsub

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"encoding/json"
	//"github.com/Shopify/sarama"
	"gopkg.in/resty.v1"
	//"github.com/elodina/go-avro"
	"github.com/linkedin/goavro"
	//"strings"
	"io/ioutil"
)

// *** Modify your function here ******************** //
func useConsumer(msg *kafka.Message, topic string, urlList []string) {
	var data interface{}
	json.Unmarshal(msg.Value, &data)

	fmt.Printf("**********\nMessage:\n%+v\n", data)
	for _,url := range urlList {
	    fmt.Printf("Send to: %s\n", url)
	    go func() {
            resty.R().
                SetBody(data).
                Post(url)
	    }()
	}
	fmt.Printf("**********\n\n")
}

func useConsumerAvro(msg *kafka.Message, topic string, avroSchema string, urlList []string) (err error) {

	/*
	msgByte := msg.Value[5:]
	m_Magic := msg.Value[0]
	m_SchemaID := msg.Value[1:4]
	m_Value := msg.Value[5:]
	fmt.Printf("Magic Byte: %#v\n", m_Magic)
	fmt.Printf("SchemaID: %#v\n", m_SchemaID)
	fmt.Printf("Record: %#v\n", m_Value)
	*/
	msgByte := msg.Value[5:]

	codec, err := goavro.NewCodec(avroSchema)
	if err != nil {
            fmt.Println(err)
	}

	var data interface{}
	data, _, err = codec.NativeFromBinary(msgByte)
	if err != nil {
            fmt.Println(err)
	}

	//fmt.Printf("Msg Consumer (bytes): %#v\n", msgByte)
	//fmt.Printf("Msg Consumer (record): %v\n", data)

	fmt.Printf("**********\nMessage:\n%+v\n", data)
	for _,url := range urlList {
	    fmt.Printf("Send to: %s\n", url)
	    go func() {
            resty.R().
                SetBody(data).
                Post(url)
	    }()
	}
	fmt.Printf("**********\n\n")
	return
}

func ConsumeKafka(topic string, group string, url []string) (err error) {

	consumer, err := kafka.NewConsumer(
		&kafka.ConfigMap{
                "bootstrap.servers":               brokers,
                "group.id":                        group,
                "session.timeout.ms":              6000,
                "go.events.channel.enable":        true,
                "go.application.rebalance.enable": true,
                "default.topic.config":            kafka.ConfigMap{"auto.offset.reset": "earliest"}})

	if err!=nil{
		fmt.Printf("Failed to create consumer: %v\n",err)
	}
	
	err = consumer.SubscribeTopics([]string{topic}, nil)
	if err!=nil{
		panic(err)
	}
	
	for {
				ev := <-consumer.Events()
				switch e := ev.(type) {
					case kafka.AssignedPartitions:
						fmt.Printf("Error: %v\n", e)
						consumer.Assign(e.Partitions)
					case kafka.RevokedPartitions:
						fmt.Printf("Error: %v\n", e)
						consumer.Unassign()
					case *kafka.Message:
						//consumer.Commit()
						useConsumer(e, topic, url)
					case kafka.PartitionEOF:
						fmt.Printf("Error: %v\n", e)
					case kafka.Error:
						fmt.Printf("Error: %v\n", e)
				}
	}
	consumer.Close()
	return
}

func ConsumeKafkaAvro(topic string, group string, avroFile string, url []string) (err error) {

	// Initiate avro schema
	var avroSchema string
	_, ok := listSchema[topic]
	if !ok {
	    avroByte, err := ioutil.ReadFile(avroFile)
            if err != nil {
	        fmt.Print(err)
	    }
	    avroSchema = string(avroByte)
	    listSchema[topic] = avroSchema
	    //fmt.Println("Create new schema")
	} else {
	    avroSchema = listSchema[topic]
	    //fmt.Println("Use existing schema")
	}

	consumer, err := kafka.NewConsumer(
		&kafka.ConfigMap{
                "bootstrap.servers":               brokers,
                "group.id":                        group,
                "session.timeout.ms":              6000,
                "go.events.channel.enable":        true,
                "go.application.rebalance.enable": true,
                "default.topic.config":            kafka.ConfigMap{"auto.offset.reset": "earliest"}})

	if err!=nil{
		fmt.Printf("Failed to create consumer: %v\n",err)
	}

	err = consumer.SubscribeTopics([]string{topic}, nil)
	if err!=nil{
		panic(err)
	}

	for {
				ev := <-consumer.Events()
				switch e := ev.(type) {
					case kafka.AssignedPartitions:
						fmt.Printf("Error: %v\n", e)
						consumer.Assign(e.Partitions)
					case kafka.RevokedPartitions:
						fmt.Printf("Error: %v\n", e)
						consumer.Unassign()
					case *kafka.Message:
						//consumer.Commit()
						useConsumerAvro(e, topic, avroSchema, url)
					case kafka.PartitionEOF:
						fmt.Printf("Error: %v\n", e)
					case kafka.Error:
						fmt.Printf("Error: %v\n", e)
				}
	}
	consumer.Close()
	return
}
