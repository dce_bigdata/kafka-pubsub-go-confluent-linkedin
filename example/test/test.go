package main

import (
	//"fmt"
	"sync"
	. "../../kafka-pubsub"
)

func ConsumeKafkaWrapper(topic string, group string, url []string) {
        var wg sync.WaitGroup
        wg.Add(1)
        go func() {
                defer wg.Done()
                ConsumeKafka(topic, group, url)
        }()
        wg.Wait()
}

func main() {

  topic := "test2"

  // Producer
  /*
  data := make(map[string]interface{})
  data["name"] = "Arditto"
  data["age"] = 27
  */
  type Message struct {
    Name string `json="name"`
    Age int `json="age"`
  }
  data := Message{Name:"Data dalam", Age:57}

  InitKafkaProducer()
  ProduceKafka(topic,data)

  // Consumer
  url := []string{}
  ConsumeKafkaWrapper(topic, "test-group", url)
}
