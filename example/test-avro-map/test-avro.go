package main

import (
	"sync"
	. "../../kafka-pubsub"
)

func ConsumeKafkaAvroWrapper(topic string, group string, avroFile string, url []string) {
        var wg sync.WaitGroup
        wg.Add(1)
        go func() {
                defer wg.Done()
                ConsumeKafkaAvro(topic, group, avroFile, url)
        }()
        wg.Wait()
}

func main() {


  //Producer

  // Data Input has map type
  topic := "test4"
  avroFile := "../avro-schema/test4.avsc"
  data := make(map[string]interface{})
  data["name"] = "Arditto"
  data["age"] = 27

  InitKafkaProducerAvro(topic, avroFile)
  ProduceKafkaAvro(topic,data)

  // Consumer
  url := []string{}
  ConsumeKafkaAvroWrapper(topic, "test-group", avroFile, url)

}
